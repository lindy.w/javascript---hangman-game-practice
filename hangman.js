"use strict";
// array for hang man secret words
var wordBank = ["Aung San Suu Kyi", "Mohandas Gandhi", "Mao Zedong", "Winston Churchill", "Genghis Khan",
				"Nelson Mandela", "Abraham Lincoln", "Adolf Hitler", "Ram Nath Kovind"];
// A-Z (virtual keyboard)
var inputs = [['Q','W','E','R','T','Y','U','I','O','P'],
   			  ['A','S','D','F','G','H','J','K','L'],
			  ['Z','X','C','V','B','N','M']
             ]
// random secret Answer
var secretAns, index;
//descriptions
var chances = 10;
var currentChances = chances;
var totalNumbers = 0; var totalCorrect = 0;

// Draw Secret Word Question (gBoard) & virtual Keyboard (gInput)
function setGame()
{
	// reset
    currentChances = chances;
    totalNumbers = totalCorrect = 0;
    $('#pic').removeAttr('src');// Remove the src
    $('#pic').text(""); $("#message").text(""); // empty text
    $('#hangman').attr("src","res/h0.jpg");

	var gWord = document.getElementById("gameBoard");
	var gInput = document.getElementById("keyInput");
	// table for secret word
	var innerStr = '';
	index = Math.floor(Math.random()*wordBank.length);
    secretAns = wordBank[index];
    innerStr += "<tr class='guessWord'>";
	for (var i=0; i < secretAns.length; i++)
	{
		if (secretAns[i] !=" ")
		{
            innerStr += "<td class='guess'></td>";
            totalNumbers++;
		}
		else
		{
			innerStr += "<td class='space'></td>"
		}
	}
    innerStr += "</tr>";
	gWord.innerHTML = innerStr;
	// table for virtual keyboard buttons
	innerStr = '';
	for (var i=0;i<inputs.length;i++)
	{
		innerStr += "<tr>";
		for (var j=0;j<inputs[i].length;j++)
		{
            innerStr += "<td class='" + inputs[i][j] + "' " + "onClick='checkInput(this)'" + ">" + inputs[i][j] + "</td>";
		}
		innerStr += "</tr>";
        gInput.innerHTML = innerStr;
	}
	// Description box
    $("#chance").text("Chances left: " + currentChances + " / " + chances);
    $("#chance").css("boarder","1px solid");
}

// check player input guess
function checkInput(letter)
{
    // remove the letter key from virtual keyboard
	$(letter).removeAttr('onclick');
    $(letter).css("color", "white");
	var isCorrect = false;

    for (var i=0; i < secretAns.length; i++)
	{
		// Correct Guess
        if (secretAns.charAt(i).toUpperCase() == $(letter).text())
        {
        	var tr = document.getElementsByClassName("guessWord")[0];
        	var tds = tr.getElementsByTagName("td");
			// display the correct guessed letter
        	tds[i].className = "correct";
            tds[i].style.backgroundColor = "#7ed14f";
            tds[i].innerHTML = $(letter).text();
            isCorrect = true;
            totalCorrect++;
        }
        else
		{
			if (isCorrect == false) isCorrect = false;
		}
	}
    // Wrong Guess
	if (isCorrect != true)
	{
        // update the chances left
        currentChances--;
        changeHangMan();
        document.getElementById("chance").innerHTML = "Chances left: " + currentChances + " / " + chances;
   		if (currentChances == 0)		 // Finish game (Lose)
		{
            $('#message').css('color', 'red');
            $('#message').text('Sorry, You Lose!');
            showAnswer();
		}
    }
    if (totalCorrect == totalNumbers)     // Finish game (WIN)
	{
		var src = "res/" + index + ".jpg";
		$('#pic').attr('src',src);
        $('#message').css('color', '#7ed14f');
        $('#message').text('Congratulations, You Survive!');
	}
}
// Showing secret answer if player failed to guess all the letter from the word
function showAnswer()
{
    for (var i=0; i < secretAns.length; i++)
    {
        var tr = document.getElementsByClassName("guessWord")[0];
        var tds = tr.getElementsByTagName("td");
        // display the remaining letter(s)
		if (tds[i].className == "guess")
		{
            tds[i].className = "wrong";
            tds[i].style.backgroundColor = "red";
            tds[i].innerHTML = secretAns[i].toUpperCase();
		}
    }
}

function changeHangMan()
{
	var image_src = "res/h" + (chances - currentChances) + ".jpg";
	$('#hangman').attr("src", image_src);
}

